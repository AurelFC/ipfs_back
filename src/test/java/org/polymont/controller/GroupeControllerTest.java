package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.GroupeDTO;
import org.polymont.repositories.GroupeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class GroupeControllerTest {

	@Autowired
	private GroupeController groupeController;

	@Autowired
	private GroupeRepository groupeRepo;

	@TestConfiguration
	static class GroupeControllerTestConfiguration {

		@Bean
		public GroupeController groupeControllerTest() {
			return new GroupeController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("GroupeControllerTest - post");

		GroupeDTO newGroupe = new GroupeDTO(0L,"SMACL", "/groupes/4/utilisateurs/","/groupes/4/fichiers/");
		GroupeDTO comparatif = groupeController.add(newGroupe);
		GroupeDTO veriteTerrain = new GroupeDTO(comparatif.getId(),"SMACL","/groupes/" + comparatif.getId() + "/utilisateurs/","/groupes/" + comparatif.getId() + "/fichiers/");

		log.info("NewGroupe : " + newGroupe);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("GroupeControllerTest - getSingle");
		GroupeDTO veriteTerrain = new GroupeDTO(1L, "MAPA", "/groupes/1/utilisateurs/", "/groupes/1/fichiers/");
		GroupeDTO comparatif = groupeController.getById(1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("GroupeControllerTest - update");
		GroupeDTO veriteTerrain = new GroupeDTO(1L, "SMACL", "/groupes/1/utilisateurs/", "/groupes/1/fichiers/");

		GroupeDTO update = new GroupeDTO(0L, "SMACL", "/groupes/1/utilisateurs/", "/groupes/1/fichiers/");
		GroupeDTO comparatif = groupeController.update(update, 1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("GroupeControllerTest - getAll");

		List<GroupeDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new GroupeDTO(1L, "MAPA", "/groupes/1/utilisateurs/", "/groupes/1/fichiers/"));
		veriteTerrain.add(new GroupeDTO(2L, "MACIF", "/groupes/2/utilisateurs/", "/groupes/2/fichiers/"));
		veriteTerrain.add(new GroupeDTO(3L, "ARKEA", "/groupes/3/utilisateurs/", "/groupes/3/fichiers/"));
		veriteTerrain.add(new GroupeDTO(4L, "COMMERCIAUX", "/groupes/4/utilisateurs/", "/groupes/4/fichiers/"));
		
		List<GroupeDTO> comparatif = groupeController.getAll();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("GroupeControllerTest - delete");

		groupeController.delete(1l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> groupeRepo.getOne(1l));
	}
}
