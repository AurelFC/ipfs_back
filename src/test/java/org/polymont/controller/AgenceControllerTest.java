package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.AgenceDTO;
import org.polymont.repositories.AgenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class AgenceControllerTest {

	@Autowired
	private AgenceController agenceController;

	@Autowired
	private AgenceRepository agenceRepo;

	@TestConfiguration
	static class AgenceControllerTestConfiguration {

		@Bean
		public AgenceController agenceControllerTest() {
			return new AgenceController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("AgenceControllerTest - post");

		AgenceDTO newAgence = new AgenceDTO(0L,"PARIS", "/agences/8/utilisateurs/", "/agences/8/fichiers/");
		AgenceDTO comparatif = agenceController.add(newAgence);
		AgenceDTO veriteTerrain = new AgenceDTO(comparatif.getId(),"PARIS","/agences/" + comparatif.getId() + "/utilisateurs/", "/agences/" + comparatif.getId() + "/fichiers/");

		log.info("NewAgence : " + newAgence);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("AgenceControllerTest - getSingle");
		AgenceDTO veriteTerrain = new AgenceDTO(5L, "NIORT", "/agences/5/utilisateurs/", "/agences/5/fichiers/");
		AgenceDTO comparatif = agenceController.getById(5L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("AgenceControllerTest - update");
		AgenceDTO veriteTerrain = new AgenceDTO(5L, "NIORT", "/agences/5/utilisateurs/", "/agences/5/fichiers/");

		AgenceDTO update = new AgenceDTO(0L, "NIORT", "/agences/5/utilisateurs/", "/agences/5/fichiers/");
		AgenceDTO comparatif = agenceController.update(update, 5L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("AgenceControllerTest - getAll");

		List<AgenceDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new AgenceDTO(5L, "NIORT", "/agences/5/utilisateurs/", "/agences/5/fichiers/"));
		veriteTerrain.add(new AgenceDTO(6L, "BREST", "/agences/6/utilisateurs/", "/agences/6/fichiers/"));
		veriteTerrain.add(new AgenceDTO(7L, "NANTES", "/agences/7/utilisateurs/", "/agences/7/fichiers/"));

		List<AgenceDTO> comparatif = agenceController.getAll();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("AgenceControllerTest - delete");

		agenceController.delete(5l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> agenceRepo.getOne(5l));
	}
}
