package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.TypeNotificationDTO;
import org.polymont.repositories.TypeNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class TypeNotificationControllerTest {

	@Autowired
	private TypeNotificationController typeNotificationController;

	@Autowired
	private TypeNotificationRepository typeNotificationRepo;

	@TestConfiguration
	static class TypeNotificationControllerTestConfiguration {

		@Bean
		public TypeNotificationController typeNotificationControllerTest() {
			return new TypeNotificationController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("TypeNotificationControllerTest - post");

		TypeNotificationDTO newTypeNotification = new TypeNotificationDTO(0L,"Partage");
		TypeNotificationDTO comparatif = typeNotificationController.add(newTypeNotification);
		TypeNotificationDTO veriteTerrain = new TypeNotificationDTO(comparatif.getId(),"Partage");

		log.info("NewTypeNotification : " + newTypeNotification);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("TypeNotificationControllerTest - getSingle");
		TypeNotificationDTO veriteTerrain = new TypeNotificationDTO(1L, "Partage");
		TypeNotificationDTO comparatif = typeNotificationController.getById(1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("TypeNotificationControllerTest - update");
		TypeNotificationDTO veriteTerrain = new TypeNotificationDTO(1L, "Partage");

		TypeNotificationDTO update = new TypeNotificationDTO(0L, "Partage");
		TypeNotificationDTO comparatif = typeNotificationController.update(update, 1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("TypeNotificationControllerTest - getAll");

		List<TypeNotificationDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new TypeNotificationDTO(1L, "Partage"));
		veriteTerrain.add(new TypeNotificationDTO(2L, "Systeme"));
		veriteTerrain.add(new TypeNotificationDTO(3L, "Personnelle"));

		List<TypeNotificationDTO> comparatif = typeNotificationController.getAll();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("TypeNotificationControllerTest - delete");

		typeNotificationController.delete(1l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> typeNotificationRepo.getOne(1l));
	}
}
