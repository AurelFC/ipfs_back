package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.FichierDTO;
import org.polymont.repositories.FichierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class FichierControllerTest {

	@Autowired
	private FichierController fichierController;

	@Autowired
	private FichierRepository fichierRepo;

	@TestConfiguration
	static class FichierControllerTestConfiguration {

		@Bean
		public FichierController fichierControllerTest() {
			return new FichierController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("FichierControllerTest - post");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2019, 01, 21, 12, 00, 01);

		FichierDTO newFichier = new FichierDTO(0L,"test","adadfaf118" ,"1", dateNotif,"pdf" ,"/groupes/1", "/fichiers/1/historiques/");
		FichierDTO comparatif = fichierController.add(newFichier);
		FichierDTO veriteTerrain = new FichierDTO(comparatif.getId(),"test","adadfaf118" ,"1", dateNotif,"pdf" ,"/groupes/1", "/fichiers/" +comparatif.getId() +"/historiques/");

		log.info("NewFichier : " + newFichier);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("FichierControllerTest - getSingle");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2020, 0, 21, 12, 00, 01);
		dateNotif.set(Calendar.MILLISECOND,0);
		FichierDTO veriteTerrain = new FichierDTO(1L, "test","adadfaf15" ,"1", dateNotif,"pdf" ,"/groupes/1", "/fichiers/1/historiques/");
		FichierDTO comparatif = fichierController.getById(1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("FichierControllerTest - update");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2018, 01, 21, 12, 00, 01);
		FichierDTO veriteTerrain = new FichierDTO(1L, "test","adadfaf15" ,"1", dateNotif, "pdf" ,"/groupes/1", "/fichiers/1/historiques/");

		FichierDTO update = new FichierDTO(0L, "test","adadfaf15" ,"1", dateNotif, "pdf" ,"/groupes/1", "/fichiers/1/historiques/");
		FichierDTO comparatif = fichierController.update(update, 1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("FichierControllerTest - getAll");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2020, 0, 21, 12, 00, 01);
		dateNotif.set(Calendar.MILLISECOND,0);
		List<FichierDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new FichierDTO(1L,  "test","adadfaf15" ,"1", dateNotif,"pdf" ,"/groupes/1", "/fichiers/1/historiques/"));
		veriteTerrain.add(new FichierDTO(2L,  "dfv","adadfaf16" ,"1", dateNotif,"pdf" ,"/groupes/2", "/fichiers/2/historiques/"));
		veriteTerrain.add(new FichierDTO(3L,  "tesvfvt","adadfaf17" ,"1", dateNotif,"pdf" ,"/groupes/3", "/fichiers/3/historiques/"));
		veriteTerrain.add(new FichierDTO(4L,  "tagada0","adadfaf18" ,"1", dateNotif,"csv" ,"/agences/5", "/fichiers/4/historiques/"));
		veriteTerrain.add(new FichierDTO(5L,  "tagada","adadfaf19" ,"1", dateNotif,"csv" ,"/utilisateurs/18", "/fichiers/5/historiques/"));

		List<FichierDTO> comparatif = fichierController.getAll(null);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("FichierControllerTest - delete");

		fichierController.delete(1l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> fichierRepo.getOne(1l));
	}
}
