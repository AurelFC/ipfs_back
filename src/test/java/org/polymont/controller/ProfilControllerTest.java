package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.ProfilDTO;
import org.polymont.repositories.ProfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class ProfilControllerTest {

	@Autowired
	private ProfilController profilController;

	@Autowired
	private ProfilRepository profilRepo;

	@TestConfiguration
	static class ProfilControllerTestConfiguration {

		@Bean
		public ProfilController profilControllerTest() {
			return new ProfilController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("ProfilControllerTest - post");

		ProfilDTO newProfil = new ProfilDTO(0L,"NEW", true, "/profils/1/utilisateurs","/profils/1/privileges");
		ProfilDTO comparatif = profilController.add(newProfil);
		ProfilDTO veriteTerrain = new ProfilDTO(comparatif.getId(),"NEW", true, "/profils/4/utilisateurs","/profils/4/privileges");

		log.info("NewProfil : " + newProfil);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("ProfilControllerTest - getSingle");
		ProfilDTO veriteTerrain = new ProfilDTO(1L, "ADMIN", true, "/profils/1/utilisateurs","/profils/1/privileges");
		ProfilDTO comparatif = profilController.getById(1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("ProfilControllerTest - update");
		ProfilDTO veriteTerrain = new ProfilDTO(1L, "ADMIN", true, "/profils/1/utilisateurs","/profils/1/privileges");

		ProfilDTO update = new ProfilDTO(0L, "ADMIN", true, "/profils/1/utilisateurs","/profils/1/privileges");
		ProfilDTO comparatif = profilController.update(update, 1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("ProfilControllerTest - getAll");

		List<ProfilDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new ProfilDTO(1L, "ADMIN", true, "/profils/1/utilisateurs","/profils/1/privileges"));
		veriteTerrain.add(new ProfilDTO(2L, "USER", true, "/profils/2/utilisateurs","/profils/2/privileges"));
		veriteTerrain.add(new ProfilDTO(3L, "GUEST", true,"/profils/3/utilisateurs","/profils/3/privileges"));

		List<ProfilDTO> comparatif = profilController.getAll();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("ProfilControllerTest - delete");

		profilController.delete(1l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> profilRepo.getOne(1l));
	}
}
