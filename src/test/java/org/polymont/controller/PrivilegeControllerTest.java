package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.repositories.PrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class PrivilegeControllerTest {

	@Autowired
	private PrivilegeController privilegeController;

	@Autowired
	private PrivilegeRepository privilegeRepo;

	@TestConfiguration
	static class PrivilegeControllerTestConfiguration {

		@Bean
		public PrivilegeController privilegeControllerTest() {
			return new PrivilegeController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("PrivilegeControllerTest - post");

		PrivilegeDTO newPrivilege = new PrivilegeDTO(0L,"TEST");
		PrivilegeDTO comparatif = privilegeController.add(newPrivilege);
		PrivilegeDTO veriteTerrain = new PrivilegeDTO(comparatif.getId(),"TEST");

		log.info("NewPrivilege : " + newPrivilege);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("PrivilegeControllerTest - getSingle");
		PrivilegeDTO veriteTerrain = new PrivilegeDTO(1L, "VISUALISER");
		PrivilegeDTO comparatif = privilegeController.getById(1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("PrivilegeControllerTest - update");
		PrivilegeDTO veriteTerrain = new PrivilegeDTO(1L, "VISUALISER");

		PrivilegeDTO update = new PrivilegeDTO(0L, "VISUALISER");
		PrivilegeDTO comparatif = privilegeController.update(update, 1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("PrivilegeControllerTest - getAll");

		List<PrivilegeDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new PrivilegeDTO(1L, "VISUALISER"));
		veriteTerrain.add(new PrivilegeDTO(2L, "TELECHARGER"));
		veriteTerrain.add(new PrivilegeDTO(3L, "UPLOADER"));
		veriteTerrain.add(new PrivilegeDTO(4L, "SUPPRIMER"));
		veriteTerrain.add(new PrivilegeDTO(5L, "PARTAGER"));

		List<PrivilegeDTO> comparatif = privilegeController.getAll();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("PrivilegeControllerTest - delete");

		privilegeController.delete(1l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> privilegeRepo.getOne(1l));
	}
}
