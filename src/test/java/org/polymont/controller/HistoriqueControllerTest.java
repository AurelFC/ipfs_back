package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.repositories.HistoriqueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class HistoriqueControllerTest {

	@Autowired
	private HistoriqueController historiqueController;

	@Autowired
	private HistoriqueRepository historiqueRepo;

	@TestConfiguration
	static class HistoriqueControllerTestConfiguration {

		@Bean
		public HistoriqueController historiqueControllerTest() {
			return new HistoriqueController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("HistoriqueControllerTest - post");

		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2018, 01, 21, 12, 00, 01);
		HistoriqueDTO newHistorique = new HistoriqueDTO(0L, dateNotif, "ceci est un historique", "/fichiers/1");
		HistoriqueDTO comparatif = historiqueController.add(newHistorique);
		HistoriqueDTO veriteTerrain = new HistoriqueDTO(comparatif.getId(), dateNotif, "ceci est un historique",
				"/fichiers/1");

		log.info("NewHistorique : " + newHistorique);
		log.info("VeriteTerrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("HistoriqueControllerTest - getSingle");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2018, 00, 21, 12, 00, 01);
		dateNotif.set(Calendar.MILLISECOND, 0);
		HistoriqueDTO veriteTerrain = new HistoriqueDTO(1L, dateNotif, "ceci est un historique", "/fichiers/1");
		HistoriqueDTO comparatif = historiqueController.getById(1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("HistoriqueControllerTest - update");

		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2018, 01, 21, 12, 00, 01);
		HistoriqueDTO veriteTerrain = new HistoriqueDTO(1L, dateNotif, "ceci est un historique", "/fichiers/1");

		HistoriqueDTO update = new HistoriqueDTO(0L, dateNotif, "ceci est un historique", "/fichiers/1");
		HistoriqueDTO comparatif = historiqueController.update(update, 1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("HistoriqueControllerTest - getAll");

		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2018, 00, 21, 12, 00, 01);
		dateNotif.set(Calendar.MILLISECOND, 0);
		Calendar dateNotif2 = Calendar.getInstance();
		dateNotif2.set(2020, 00, 21, 12, 00, 01);
		dateNotif2.set(Calendar.MILLISECOND, 0);
		Calendar dateNotif3 = Calendar.getInstance();
		dateNotif3.set(2020, 01, 21, 12, 00, 01);
		dateNotif3.set(Calendar.MILLISECOND, 0);
		Calendar dateNotif4 = Calendar.getInstance();
		dateNotif4.set(2020, 02, 21, 12, 00, 01);
		dateNotif4.set(Calendar.MILLISECOND, 0);

		List<HistoriqueDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new HistoriqueDTO(1L, dateNotif, "ceci est un historique", "/fichiers/1"));
		veriteTerrain.add(new HistoriqueDTO(2L, dateNotif3, "ceci est un historique", "/fichiers/1"));
		veriteTerrain.add(new HistoriqueDTO(3L, dateNotif4, "ceci est un historique", "/fichiers/2"));
		veriteTerrain.add(new HistoriqueDTO(4L, dateNotif2, "ceci est un historique", "/fichiers/2"));
		veriteTerrain.add(new HistoriqueDTO(5L, dateNotif3, "ceci est un historique", "/fichiers/3"));
		veriteTerrain.add(new HistoriqueDTO(6L, dateNotif2, "ceci est un historique", "/fichiers/3"));

		List<HistoriqueDTO> comparatif = historiqueController.getAll();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("HistoriqueControllerTest - delete");

		historiqueController.delete(1l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> historiqueRepo.getOne(1l));
	}
}
