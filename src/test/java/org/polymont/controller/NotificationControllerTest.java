package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.NotificationDTO;
import org.polymont.repositories.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class NotificationControllerTest {

	@Autowired
	private NotificationController notificationController;

	@Autowired
	private NotificationRepository notificationRepo;

	@TestConfiguration
	static class NotificationControllerTestConfiguration {

		@Bean
		public NotificationController notificationControllerTest() {
			return new NotificationController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("NotificationControllerTest - post");

		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2019, 01, 21, 12, 00, 01);

		NotificationDTO newNotification = new NotificationDTO(0L, "Vous avez partagé le fichier X", dateNotif, true,
				"/utilisateurs/8", "/typeNotifications/1");
		NotificationDTO comparatif = notificationController.add(newNotification);
		NotificationDTO veriteTerrain = new NotificationDTO(comparatif.getId(), "Vous avez partagé le fichier X",
				dateNotif, true, "/utilisateurs/8", "/typeNotifications/1");

		log.info("NewNotification : " + newNotification);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("NotificationControllerTest - getSingle");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2019, 01, 21, 12, 00, 01);
		dateNotif.set(Calendar.MILLISECOND, 0);
		NotificationDTO veriteTerrain = new NotificationDTO(1L, "Vous avez partagé le fichier X", dateNotif, true,
				"/utilisateurs/8", "/typeNotifications/1");
		NotificationDTO comparatif = notificationController.getById(1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("NotificationControllerTest - update");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2019, 01, 21, 12, 00, 01);
		NotificationDTO veriteTerrain = new NotificationDTO(1L, "Vous avez partagé le fichier X", dateNotif, true,
				"/utilisateurs/8", "/typeNotifications/1");

		NotificationDTO update = new NotificationDTO(0L, "Vous avez partagé le fichier X", dateNotif, true,
				"/utilisateurs/8", "/typeNotifications/1");
		NotificationDTO comparatif = notificationController.update(update, 1L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("NotificationControllerTest - getAll");
		Calendar dateNotif = Calendar.getInstance();
		dateNotif.set(2019, 01, 21, 12, 0, 01);
		dateNotif.set(Calendar.MILLISECOND, 0);
		Calendar dateNotif2 = Calendar.getInstance();
		dateNotif2.set(2019, 02, 21, 12, 0, 01);
		dateNotif2.set(Calendar.MILLISECOND, 0);
		List<NotificationDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new NotificationDTO(1L, "Vous avez partagé le fichier X", dateNotif, true, "/utilisateurs/8",
				"/typeNotifications/1"));
		veriteTerrain.add(new NotificationDTO(2L, "Changement de mot de passe effectué", dateNotif, true,
				"/utilisateurs/9", "/typeNotifications/2"));
		veriteTerrain.add(new NotificationDTO(3L, "Michel a partagé le fichier X", dateNotif2, false, "/utilisateurs/10",
				"/typeNotifications/1"));
		veriteTerrain.add(new NotificationDTO(4L, "Vous avez rejoint le groupe RH", dateNotif, false, "/utilisateurs/11",
				"/typeNotifications/3"));
		veriteTerrain.add(new NotificationDTO(5L, "Vous avez partagé le fichier Y", dateNotif, true, "/utilisateurs/12",
				"/typeNotifications/1"));
		veriteTerrain.add(new NotificationDTO(6L, "Jean-Pierre a mis à jour le fichier Y", dateNotif2, true,
				"/utilisateurs/13", "/typeNotifications/1"));

		List<NotificationDTO> comparatif = notificationController.getAll();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("NotificationControllerTest - delete");

		notificationController.delete(1l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> notificationRepo.getOne(1l));
	}
}
