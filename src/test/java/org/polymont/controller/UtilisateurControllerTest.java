package org.polymont.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import lombok.extern.log4j.Log4j2;

@DataJpaTest
@AutoConfigureTestEntityManager
@ExtendWith(SpringExtension.class)
@Log4j2
public class UtilisateurControllerTest {

	@Autowired
	private UtilisateurController utilisateurController;

	@Autowired
	private UtilisateurRepository utilisateurRepo;

	@TestConfiguration
	static class UtilisateurControllerTestConfiguration {

		@Bean
		public UtilisateurController utilisateurControllerTest() {
			return new UtilisateurController();
		}
	}

	@Test
	public void post() throws Exception {
		log.info("UtilisateurControllerTest - post");

		UtilisateurDTO newUtilisateur = new UtilisateurDTO(0L, "Lemetayer", "Marc", "marc.lemetayer@polymont.fr",
				"0505050505", "/profils/2", "", "", "");
		UtilisateurDTO comparatif = utilisateurController.add(newUtilisateur);
		UtilisateurDTO veriteTerrain = new UtilisateurDTO(comparatif.getId(), "Lemetayer", "Marc",
				"marc.lemetayer@polymont.fr", "0505050505", "/profils/2",
				"/utilisateurs/" + comparatif.getId() + "/fichiers/",
				"/utilisateurs/" + comparatif.getId() + "/agences/",
				"/utilisateurs/" + comparatif.getId() + "/groupes/");

		log.info("NewUtilisateur : " + newUtilisateur);
		log.info("VeriteTerrain : " + veriteTerrain);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingle() throws Exception {
		log.info("UtilisateurControllerTest - getSingle");
		UtilisateurDTO veriteTerrain = new UtilisateurDTO(8L, "Dromer", "Axel", "axel.dromer@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/8/fichiers/", "/utilisateurs/8/agences/", "/utilisateurs/8/groupes/");
		UtilisateurDTO comparatif = utilisateurController.getById(8L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getSingleByMail() throws Exception {
		log.info("EmployeControllerTest - getSingleByMail");
		UtilisateurDTO veriteTerrain = new UtilisateurDTO(8L, "Dromer", "Axel", "axel.dromer@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/8/fichiers/", "/utilisateurs/8/agences/", "/utilisateurs/8/groupes/");

		UtilisateurDTO comparatif = utilisateurController.getByEmail("axel.dromer@polymont.fr");

		log.info("Vérite Terrain : " + veriteTerrain);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void update() throws Exception {
		log.info("UtilisateurControllerTest - update");
		UtilisateurDTO veriteTerrain = new UtilisateurDTO(8L, "Dromer", "Axel", "axel.dromer@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/8/fichiers/", "/utilisateurs/8/agences/", "/utilisateurs/8/groupes/");

		UtilisateurDTO update = new UtilisateurDTO(0L, "Dromer", "Axel", "axel.dromer@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/8/fichiers/", "/utilisateurs/8/agences/", "/utilisateurs/8/groupes/");
		UtilisateurDTO comparatif = utilisateurController.update(update, 8L);

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(veriteTerrain, comparatif);
	}

	@Test
	public void getAll() throws Exception {
		log.info("UtilisateurControllerTest - getAll");

		List<UtilisateurDTO> veriteTerrain = new ArrayList<>();
		veriteTerrain.add(new UtilisateurDTO(8L, "Dromer", "Axel", "axel.dromer@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/8/fichiers/", "/utilisateurs/8/agences/", "/utilisateurs/8/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(9L, "Guiard", "Elodie", "elodie.guiard@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/9/fichiers/", "/utilisateurs/9/agences/", "/utilisateurs/9/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(10L, "Perron", "Mathis", "mathis.perron@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/10/fichiers/", "/utilisateurs/10/agences/", "/utilisateurs/10/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(11L, "Bremaud", "Adrien", "adrien.bremaud@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/11/fichiers/", "/utilisateurs/11/agences/", "/utilisateurs/11/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(12L, "Delahaye", "Guillaume", "guillaume.delahaye@polymont.fr",
				"0505050505", "/profils/2", "/utilisateurs/12/fichiers/", "/utilisateurs/12/agences/",
				"/utilisateurs/12/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(13L, "Rousseau", "Nicolas", "nicolas.rousseau@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/13/fichiers/", "/utilisateurs/13/agences/", "/utilisateurs/13/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(14L, "Rodriguez", "Christian", "christian.rodriguez@polymont.fr",
				"0505050505", "/profils/2", "/utilisateurs/14/fichiers/", "/utilisateurs/14/agences/",
				"/utilisateurs/14/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(15L, "Bendi-Ouis", "Yannis", "y.bendi-ouis@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/15/fichiers/", "/utilisateurs/15/agences/", "/utilisateurs/15/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(16L, "Bouharb", "Wael", "wael.bouharb@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/16/fichiers/", "/utilisateurs/16/agences/", "/utilisateurs/16/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(17L, "Beerman", "Joker", "thibaud.moulet@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/17/fichiers/", "/utilisateurs/17/agences/", "/utilisateurs/17/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(18L, "Fuerte-Canivet", "Aurelien", "a.fuerte-canivet@polymont.fr",
				"0505050505", "/profils/1", "/utilisateurs/18/fichiers/", "/utilisateurs/18/agences/",
				"/utilisateurs/18/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(19L, "Gelineau", "Jeremy", "jeremy.gelineau@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/19/fichiers/", "/utilisateurs/19/agences/", "/utilisateurs/19/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(20L, "Andre", "Romuald", "romuald.andre@polymont.fr", "0505050505",
				"/profils/2", "/utilisateurs/20/fichiers/", "/utilisateurs/20/agences/", "/utilisateurs/20/groupes/"));
		veriteTerrain.add(new UtilisateurDTO(21L, "Bobby", "Michel", "bobby@polymont.fr", "0505050505", "/profils/3",
				"/utilisateurs/21/fichiers/", "/utilisateurs/21/agences/", "/utilisateurs/21/groupes/"));

		List<UtilisateurDTO> comparatif = utilisateurController.getAllUtilisateurs();

		log.info("Vérite Terrain : " + veriteTerrain);
		log.info("Comparatif : " + comparatif);

		assertEquals(comparatif, veriteTerrain);
	}

	@Test
	public void delete() throws Exception {
		log.info("UtilisateurControllerTest - delete");

		utilisateurController.delete(8l);
		log.info("Vérite Terrain : Suppression");

		assertThrows(JpaObjectRetrievalFailureException.class, () -> utilisateurRepo.getOne(8l));
	}
}
