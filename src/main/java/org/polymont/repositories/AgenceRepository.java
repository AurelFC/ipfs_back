package org.polymont.repositories;

import java.util.List;

import org.polymont.entities.Agence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgenceRepository extends JpaRepository<Agence, Long> {
	public List<Agence> getByUtilisateursId(long utilisateurId);
}
