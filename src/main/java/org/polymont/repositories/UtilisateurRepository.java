package org.polymont.repositories;

import java.util.List;
import java.util.Set;

import org.polymont.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
	public Utilisateur getByEmailPro(String email);
	public List<Utilisateur> getByProfilId(long id);
	public Set<Utilisateur> getByGroupesId(long id);
	public Set<Utilisateur> getByAgencesId(long id);
}
