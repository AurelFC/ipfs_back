package org.polymont.repositories;

import org.polymont.entities.Profil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfilRepository  extends JpaRepository<Profil, Long>{

}
