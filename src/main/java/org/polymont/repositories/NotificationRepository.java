package org.polymont.repositories;

import java.util.List;

import org.polymont.entities.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, Long>{
	public List<Notification> getByUtilisateurId(long utilisateurId);
}
