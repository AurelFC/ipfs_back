package org.polymont.repositories;

import java.util.List;

import org.polymont.entities.Fichier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FichierRepository extends JpaRepository<Fichier, Long> {
	public List<Fichier> getByOwnerId(long ownerId);
}
