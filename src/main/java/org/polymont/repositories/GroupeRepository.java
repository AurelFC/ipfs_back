package org.polymont.repositories;

import java.util.List;

import org.polymont.entities.Groupe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupeRepository extends JpaRepository<Groupe, Long> {
	public List<Groupe> getByUtilisateursId(long utilisateurId);
}
