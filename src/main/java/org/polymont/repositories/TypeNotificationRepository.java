package org.polymont.repositories;

import org.polymont.entities.TypeNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeNotificationRepository extends JpaRepository<TypeNotification, Long> {

}
