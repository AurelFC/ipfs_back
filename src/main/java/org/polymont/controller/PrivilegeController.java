package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.polymont.converter.PrivilegeConverter;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.entities.Privilege;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.PrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/privileges")
@CrossOrigin(origins = "*")
@Log4j2
public class PrivilegeController {

	@Autowired
	PrivilegeRepository privilegeRepository;

	@Autowired
	PrivilegeConverter privilegeConverter;

	public PrivilegeController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PrivilegeDTO> getAll() {
		log.info("GET PrivilegesAll - ");
		List<PrivilegeDTO> privilegesDtos = new ArrayList<>();
		privilegeRepository.findAll().forEach(action -> privilegesDtos.add(privilegeConverter.convertToDto(action)));
		return privilegesDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PrivilegeDTO getById(@PathVariable long id) {
		log.info("GET PrivilegesById - ");
		GenericEntityNotFoundException.checkById(id, privilegeRepository, Privilege.class);
		return privilegeConverter.convertToDto(privilegeRepository.getOne(id));
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter un privilege
	 * 
	 * @param PrivilegeDTO newPrivilege
	 * @return ResponseEntity<Privilege>
	 */
	@PostMapping
	public PrivilegeDTO add(@RequestBody PrivilegeDTO privilegeDto) {
		log.info("POST Privilege - ");
		Privilege privilegeEntity = privilegeConverter.convertToEntity(privilegeDto);
		try {
			privilegeRepository.saveAndFlush(privilegeEntity);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return privilegeConverter.convertToDto(privilegeEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'un privilege
	 * 
	 * @param long id - ID du privilege à mettre à jour
	 * @return ResponseEntity<Privilege>
	 */
	@PutMapping(path = "/{id}")
	public PrivilegeDTO update(@RequestBody PrivilegeDTO updatePrivilegeDTO, @PathVariable long id) {
		log.info("PUT Privilege - ");

		Privilege privilege = privilegeRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Privilege.class));
		privilege.setLibelle(updatePrivilegeDTO.getLibelle());
		return privilegeConverter.convertToDto(privilegeRepository.saveAndFlush(privilege));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer un privilege
	 * 
	 * @param long id - ID du privilege à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Privilege - ");
		try {
			privilegeRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, Privilege.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer ce privilege", e);
		}
	}

}
