package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.polymont.converter.AgenceConverter;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.AgenceDTO;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Agence;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.AgenceRepository;
import org.polymont.repositories.FichierRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/agences")
@CrossOrigin(origins = "*")
@Log4j2
public class AgenceController {

	@Autowired
	AgenceRepository agenceRepository;

	@Autowired
	AgenceConverter agenceConverter;

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	FichierRepository fichierRepository;

	@Autowired
	FichierConverter fichierConverter;

	public AgenceController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AgenceDTO> getAll() {
		List<AgenceDTO> agencesDtos = new ArrayList<>();
		agenceRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
				.forEach(agence -> agencesDtos.add(agenceConverter.convertToDto(agence)));
		return agencesDtos;

	}

	/**
	 * Méthode getUtilisateurs : retourne les utilisateurs concernant une agence
	 * 
	 * @param id
	 * @return ResponseEntity<Utilisateur>
	 */
	@GetMapping(path = "/{id}/utilisateurs", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UtilisateurDTO> getUtilisateurs(@PathVariable long id) {
		log.info("GET Utilisateurs - ");
		List<UtilisateurDTO> utilisateurs = new ArrayList<>();
		agenceRepository.getOne(id).getUtilisateurs()
				.forEach(utilisateur -> utilisateurs.add(utilisateurConverter.convertToDto(utilisateur)));
		return utilisateurs;
	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AgenceDTO getById(@PathVariable long id) {
		GenericEntityNotFoundException.checkById(id, agenceRepository, Agence.class);
		return agenceConverter.convertToDto(agenceRepository.getOne(id));
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter une agence
	 * 
	 * @param AgenceDTO newAgence
	 * @return ResponseEntity<Agence>
	 */
	@PostMapping
	public AgenceDTO add(@RequestBody AgenceDTO newAgence) {
		log.info("POST Agence - " + newAgence);
		// Enregistrement du agence dans la base
		return agenceConverter.convertToDto(agenceRepository.save(agenceConverter.convertToEntity(newAgence)));
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'une agence
	 * 
	 * @param long id - ID de l'agence à mettre à jour
	 * @return ResponseEntity<Agence>
	 */
	@PutMapping(path = "/{id}")
	public AgenceDTO update(@RequestBody AgenceDTO updateAgenceDTO, @PathVariable long id) {
		log.info("PUT Agence - " + updateAgenceDTO);

		Agence agence = agenceRepository.getOne(id);
		// Mise à jour de l'agence
		agence.setLibelle(updateAgenceDTO.getLibelle());
		return agenceConverter.convertToDto(agenceRepository.saveAndFlush(agence));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer une agence
	 * 
	 * @param long id - ID de l'agence à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Agence - ");
		agenceRepository.deleteById(id);
	}

	/**
	 * Méthode PUT permettant de retirer un utilisateur de l'agence
	 * 
	 * @param id  (correspond à l'id du groupe)
	 * @param id2 (correspond à l'id de l'utilisateur)
	 * @return
	 */
	@PutMapping(path = "/{id}/utilisateurs/{id2}")
	public AgenceDTO deleteUtilisateur(@PathVariable long id, @PathVariable long id2) {
		log.info("DELETE Utilisateur de l'agence - ");
		Agence agence = agenceRepository.getOne(id);
		agence.deleteUtilisateurs(utilisateurRepository.getOne(id2));
		return agenceConverter.convertToDto(agenceRepository.saveAndFlush(agence));
	}

	/**
	 * Méthode getHistoriques : retourne les fichiers concernant un utilisateur
	 * 
	 * @param id
	 */
	@GetMapping(path = "/{id}/fichiers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FichierDTO> getFichiers(@PathVariable long id) {
		log.info("GET Fichiers - ");
		List<FichierDTO> fichiers = new ArrayList<>();
		if (agenceRepository.existsById(id)) {
			fichierRepository.getByOwnerId(id).forEach(fichier -> fichiers.add(fichierConverter.convertToDto(fichier)));
		} else {
			throw new EntityNotFoundException("L'agence " + id + " n'existe pas.");
		}
		return fichiers;
	}

}
