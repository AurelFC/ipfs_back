package org.polymont.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.polymont.configuration.Config;
import org.polymont.converter.AgenceConverter;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.GroupeConverter;
import org.polymont.converter.NotificationConverter;
import org.polymont.converter.ProfilConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.AgenceDTO;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.GroupeDTO;
import org.polymont.dtos.NotificationDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Agence;
import org.polymont.entities.Groupe;
import org.polymont.entities.Notification;
import org.polymont.entities.TypeNotification;
import org.polymont.entities.Utilisateur;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.AgenceRepository;
import org.polymont.repositories.FichierRepository;
import org.polymont.repositories.GroupeRepository;
import org.polymont.repositories.NotificationRepository;
import org.polymont.repositories.ProfilRepository;
import org.polymont.repositories.TypeNotificationRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/utilisateurs")
@CrossOrigin(origins = "*")
@Log4j2
public class UtilisateurController {

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	FichierRepository fichierRepository;

	@Autowired
	FichierConverter fichierConverter;

	@Autowired
	AgenceRepository agenceRepository;

	@Autowired
	AgenceConverter agenceConverter;

	@Autowired
	GroupeRepository groupeRepository;

	@Autowired
	GroupeConverter groupeConverter;

	@Autowired
	ProfilRepository profilRepository;

	@Autowired
	ProfilConverter profilConverter;

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	NotificationConverter notificationConverter;

	@Autowired
	TypeNotificationRepository typeNotificationRepository;

	public UtilisateurController() {
		super();
	}

	/**
	 * Méthode GET de l'api RESTFull permettant de récupérer tous les utilisateurs
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UtilisateurDTO> getAll(@RequestParam(value = "email", required = false) String email) {
		List<UtilisateurDTO> utilisateursDtos = new ArrayList<>();
		if (email == null) {
			utilisateursDtos = getAllUtilisateurs();
		} else {
			utilisateursDtos.add(getByEmail(email));
		}
		return utilisateursDtos;
	}

	public UtilisateurDTO getByEmail(String email) {
		log.info("GET byEmail - ");
		Utilisateur utilisateur = null;
		if (utilisateurRepository.getByEmailPro(email) == null) {
			throw new GenericEntityNotFoundException(email, Utilisateur.class);
		} else {
			utilisateur = utilisateurRepository.getByEmailPro(email);
		}
		return utilisateurConverter.convertToDto(utilisateur);
	}

	public List<UtilisateurDTO> getAllUtilisateurs() {
		log.info("GET UtilisateursAll - ");
		List<UtilisateurDTO> utilisateursDtos = new ArrayList<>();
		utilisateurRepository.findAll().forEach(util -> utilisateursDtos.add(utilisateurConverter.convertToDto(util)));
		return utilisateursDtos;

	}

	/**
	 * Méthode GET de l'api RESTFull permettant de récupérer un utilisateur
	 * 
	 * @param long id - ID de l'utilisateur à récupérer
	 * @return UtilisateurDTO - ResponseEntity contenant l'utilisateur demandé
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public UtilisateurDTO getById(@PathVariable long id) {
		log.info("GET UtilisateursById - ");
		GenericEntityNotFoundException.checkById(id, utilisateurRepository, Utilisateur.class);
		return utilisateurConverter.convertToDto(utilisateurRepository.getOne(id));
	}

	/**
	 * Méthode getAgences : retourne les agences concernant un utilisateur
	 * 
	 * @param id
	 */
	@GetMapping(path = "/{id}/agences", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AgenceDTO> getAgences(@PathVariable long id) {
		log.info("GET Utilisateurs - Agences - ");
		GenericEntityNotFoundException.checkById(id, utilisateurRepository, Utilisateur.class);
		List<AgenceDTO> agences = new ArrayList<>();
		agenceRepository.getByUtilisateursId(id).forEach(agence -> agences.add(agenceConverter.convertToDto(agence)));
		return agences;
	}

	/**
	 * Méthode getGroupes : retourne les groupes concernant un utilisateur
	 * 
	 * @param id
	 */
	@GetMapping(path = "/{id}/groupes", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GroupeDTO> getGroupes(@PathVariable long id) {
		log.info("GET Utilisateurs - Groupes - ");
		GenericEntityNotFoundException.checkById(id, utilisateurRepository, Utilisateur.class);
		List<GroupeDTO> groupes = new ArrayList<>();
		groupeRepository.getByUtilisateursId(id).forEach(groupe -> groupes.add(groupeConverter.convertToDto(groupe)));
		return groupes;
	}

	/**
	 * Méthode getHistoriques : retourne les notifications concernant un utilisateur
	 * 
	 * @param id
	 */
	@GetMapping(path = "/{id}/notifications", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<NotificationDTO> getNotifications(@PathVariable long id) {
		log.info("GET Utilisateurs - notifications - ");
		GenericEntityNotFoundException.checkById(id, utilisateurRepository, Utilisateur.class);
		List<NotificationDTO> notifications = new ArrayList<>();
		notificationRepository.getByUtilisateurId(id)
				.forEach(notification -> notifications.add(notificationConverter.convertToDto(notification)));
		return notifications;
	}

	/**
	 * Méthode getHistoriques : retourne les fichiers concernant un utilisateur
	 * 
	 * @param id
	 */
	@GetMapping(path = "/{id}/fichiers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FichierDTO> getFichiers(@PathVariable long id) {
		log.info("GET Utilisateurs - Fichiers - ");
		GenericEntityNotFoundException.checkById(id, utilisateurRepository, Utilisateur.class);
		List<FichierDTO> fichiers = new ArrayList<>();
		fichierRepository.getByOwnerId(id).forEach(fichier -> fichiers.add(fichierConverter.convertToDto(fichier)));
		return fichiers;
	}

	/**
	 * Méthode POST de l'api RESTFull pour ajouter un utilisateur
	 * 
	 * @param Employe newEmploye
	 * @return newEmploye
	 */
	@PostMapping
	public UtilisateurDTO add(@RequestBody UtilisateurDTO utilisateurDto) {
		log.info("POST Utilisateur - ");
		Utilisateur utilisateurEntity = utilisateurConverter.convertToEntity(utilisateurDto);
		try {
			TypeNotification typeNo = typeNotificationRepository.getOne(3L);
			Notification notif = new Notification("Bienvenue sur IPFS", Calendar.getInstance(), false, typeNo,
					utilisateurEntity);
			utilisateurEntity.setProfil(profilRepository.getOne(Config.getIdFromUri(utilisateurDto.getProfil())));
			utilisateurEntity = utilisateurRepository.saveAndFlush(utilisateurEntity);
			notificationRepository.saveAndFlush(notif);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return utilisateurConverter.convertToDto(utilisateurEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFull pour la modification d'un utilisateur
	 * 
	 * @param UtilisateurDTO update
	 * @param long           id - ID de l'utilisateur à mettre à jour
	 * @return UtilisateurDTO
	 */
	@PutMapping(path = "/{id}")
	public UtilisateurDTO update(@RequestBody(required = true) UtilisateurDTO updateUtilisateurDTO,
			@PathVariable long id) {
		log.info("PUT Utilisateur - ");
		Utilisateur utilisateur = utilisateurRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Utilisateur.class));

		utilisateur.setNom(updateUtilisateurDTO.getNom());
		utilisateur.setPrenom(updateUtilisateurDTO.getPrenom());
		utilisateur.setEmailPro(updateUtilisateurDTO.getEmailPro());
		utilisateur.setPrenom(updateUtilisateurDTO.getPrenom());
		utilisateur.setTelPro(updateUtilisateurDTO.getTelPro());
		utilisateur.setActif(updateUtilisateurDTO.isActif());
		utilisateur.setProfil(profilRepository.getOne(Config.getIdFromUri(updateUtilisateurDTO.getProfil())));

		return utilisateurConverter.convertToDto(utilisateurRepository.saveAndFlush(utilisateur));
	}

	/**
	 * Méthode PUT pour ajouter un groupe à un utilisateur
	 * 
	 * @param id
	 * @param GroupeDTO
	 * @return
	 */

	@PutMapping(path = "/{id}/groupes", produces = MediaType.APPLICATION_JSON_VALUE)
	public UtilisateurDTO addGroupes(@PathVariable long id, @RequestBody GroupeDTO newGroupeDTO) {
		log.info("PUT Groupes ajout - ");
		Utilisateur utilisateur = utilisateurRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Utilisateur.class));
		Groupe groupe = groupeConverter.convertToEntity(newGroupeDTO);
		groupe.addUtilisateurs(utilisateur);
		groupeRepository.saveAndFlush(groupe);
		List<GroupeDTO> groupes = new ArrayList<>();
		utilisateur.getGroupes().forEach(grou -> groupes.add(groupeConverter.convertToDto(grou)));
		return utilisateurConverter.convertToDto(utilisateur);
	}

	/**
	 * Méthode PUT pour ajouter un agence à un utilisateur
	 * 
	 * @param id
	 * @param AgenceDTO
	 * @return
	 */

	@PutMapping(path = "/{id}/agences", produces = MediaType.APPLICATION_JSON_VALUE)
	public UtilisateurDTO addAgences(@PathVariable long id, @RequestBody AgenceDTO newAgenceDTO) {
		log.info("PUT Agences ajout - ");
		Utilisateur utilisateur = utilisateurRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Utilisateur.class));
		Agence agence = agenceConverter.convertToEntity(newAgenceDTO);
		agence.addUtilisateurs(utilisateur);
		agenceRepository.saveAndFlush(agence);
		List<AgenceDTO> agences = new ArrayList<>();
		utilisateur.getAgences().forEach(agen -> agences.add(agenceConverter.convertToDto(agen)));
		return utilisateurConverter.convertToDto(utilisateur);
	}

	/**
	 * Méthode DELETE de l'api RESTFull permettant de supprimer un utilisateur
	 * 
	 * @param long id - ID de l'utilisateur à supprimer
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Utilisateur - ");
		try {
			utilisateurRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, Utilisateur.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer cet utilisateur", e);
		}

	}

	/**
	 * Méthode PUT permettant de retirer une agence à l'utilisateur
	 * 
	 * @param id  (correspond à l'id de l'utilisateur)
	 * @param id2 (correspond à l'id du groupe)
	 * @return
	 */
	@PutMapping(path = "/{id}/agences/{id2}")
	public AgenceDTO deleteAgence(@PathVariable long id, @PathVariable long id2) {
		log.info("DELETE agence de l'utilisateur - ");
		Agence agence = agenceRepository.findById(id2)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Agence.class));
		agence.deleteUtilisateurs(utilisateurRepository.getOne(id));
		return agenceConverter.convertToDto(agenceRepository.saveAndFlush(agence));
	}

	/**
	 * Méthode PUT permettant de retirer un groupe à l'utilisateur
	 * 
	 * @param id  (correspond à l'id l'utilisateur )
	 * @param id2 (correspond à l'id du groupe)
	 * @return
	 */
	@PutMapping(path = "/{id}/groupes/{id2}")
	public GroupeDTO deleteGroupe(@PathVariable long id, @PathVariable long id2) {
		log.info("DELETE groupe de l'utilisateur - ");
		Groupe groupe = groupeRepository.findById(id2)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Groupe.class));
		groupe.deleteUtilisateurs(utilisateurRepository.getOne(id));
		return groupeConverter.convertToDto(groupeRepository.saveAndFlush(groupe));
	}

}
