package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.polymont.converter.HistoriqueConverter;
import org.polymont.converter.TypeNotificationConverter;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.entities.Historique;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.HistoriqueRepository;
import org.polymont.repositories.TypeNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/historiques")
@CrossOrigin(origins = "*")
@Log4j2
public class HistoriqueController {

	@Autowired
	HistoriqueRepository historiqueRepository;

	@Autowired
	HistoriqueConverter historiqueConverter;

	@Autowired
	TypeNotificationRepository typeNotificationRepository;

	@Autowired
	TypeNotificationConverter typeNotificationConverter;

	public HistoriqueController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HistoriqueDTO> getAll() {
		log.info("GET HistoriquesAll - ");
		List<HistoriqueDTO> historiquesDtos = new ArrayList<>();
		historiqueRepository.findAll()
				.forEach(historique -> historiquesDtos.add(historiqueConverter.convertToDto(historique)));
		return historiquesDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public HistoriqueDTO getById(@PathVariable long id) {
		log.info("GET HistoriquesById - ");
		GenericEntityNotFoundException.checkById(id, historiqueRepository, Historique.class);
		return historiqueConverter.convertToDto(historiqueRepository.getOne(id));
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter une historique
	 * 
	 * @param HistoriqueDTO newHistorique
	 * @return ResponseEntity<Historique>
	 */
	@PostMapping
	public HistoriqueDTO add(@RequestBody HistoriqueDTO historiqueDto) {
		log.info("POST Historique - ");
		// Enregistrement de l'historique dans la base
		Historique historiqueEntity = historiqueConverter.convertToEntity(historiqueDto);
		try {
			historiqueRepository.save(historiqueEntity);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return historiqueConverter.convertToDto(historiqueEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'une historique
	 * 
	 * @param long id - ID de l'historique à mettre à jour
	 * @return ResponseEntity<Historique>
	 */
	@PutMapping(path = "/{id}")
	public HistoriqueDTO update(@RequestBody HistoriqueDTO updateHistoriqueDTO, @PathVariable long id) {
		log.info("PUT Historique - ");

		Historique historique = historiqueRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Historique.class));
		// Mise à jour de l'historique
		historique.setDateModif(updateHistoriqueDTO.getDateModif());
		return historiqueConverter.convertToDto(historiqueRepository.saveAndFlush(historique));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer une historique
	 * 
	 * @param long id - ID de l'historique à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Historique - ");
		try {
			historiqueRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, Historique.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer cet historique", e);
		}
	}

}
