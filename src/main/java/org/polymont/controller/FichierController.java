package org.polymont.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.polymont.configuration.Config;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.HistoriqueConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.entities.Fichier;
import org.polymont.entities.Historique;
import org.polymont.entities.Owner;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.FichierRepository;
import org.polymont.repositories.HistoriqueRepository;
import org.polymont.repositories.OwnerRepository;
import org.polymont.repositories.TypeNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/fichiers")
@CrossOrigin(origins = "*")
@Log4j2
public class FichierController {

	@Autowired
	FichierRepository fichierRepository;

	@Autowired
	FichierConverter fichierConverter;

	@Autowired
	OwnerRepository ownerRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	HistoriqueRepository historiqueRepository;

	@Autowired
	HistoriqueConverter historiqueConverter;

	@Autowired
	TypeNotificationRepository typeNotificationRepository;

	public FichierController() {
		super();
	}

	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FichierDTO> getAll(@RequestParam(value = "ownerId", required = false) Long ownerId) { // objet pour qu'il puisse être null
		log.info("GET FichiersAll - ");
		final List<FichierDTO> all;
		if (ownerId == null) {
			all = getAllFichiers();
		} else {
			List<FichierDTO> byOwner = new ArrayList<>();
			fichierRepository.getByOwnerId(ownerId).forEach(fichier -> byOwner.add(fichierConverter.convertToDto(fichier)));
			all = byOwner;
		}
		return all;

	}
	
	public List<FichierDTO> getAllFichiers() {
		log.info("GET getAllFichiers - ");
		List<FichierDTO> fichiersDtos = new ArrayList<>();
		fichierRepository.findAll().forEach(fichier -> fichiersDtos.add(fichierConverter.convertToDto(fichier)));
		return fichiersDtos;
	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FichierDTO getById(@PathVariable long id) {
		log.info("GET FichiersById - ");
		GenericEntityNotFoundException.checkById(id, fichierRepository, Fichier.class);
		return fichierConverter.convertToDto(fichierRepository.getOne(id));
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter un fichier
	 * 
	 * @param FichierDTO newFichier
	 * @return ResponseEntity<Fichier>
	 */
	@PostMapping
	public FichierDTO add(@RequestBody FichierDTO newFichier) {
		log.info("POST Fichier - ");
		// Enregistrement du fichier dans la base
		Fichier fichierEntity = fichierConverter.convertToEntity(newFichier);
		try {
			fichierEntity.setOwner(ownerRepository.getOne(Config.getIdFromUri(newFichier.getOwner())));
			fichierEntity = fichierRepository.save(fichierEntity);
			Historique newHisto = new Historique(Calendar.getInstance(), "CREATION", fichierEntity);
			historiqueRepository.save(newHisto);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return fichierConverter.convertToDto(fichierEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'un fichier
	 * 
	 * @param long id - ID du fichier à mettre à jour
	 * @return ResponseEntity<Fichier>
	 */
	@PutMapping(path = "/{id}")
	public FichierDTO update(@RequestBody FichierDTO updateFichierDTO, @PathVariable long id) {
		log.info("PUT Fichier - ");
		Fichier fichier = fichierRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Fichier.class));
		List<Historique> historiques = new ArrayList<>();

		// Mise à jour du fichier
		fichier.setDateAjout(updateFichierDTO.getDateAjout());
		fichier.setExtension(updateFichierDTO.getExtension());
		fichier.setHash(updateFichierDTO.getHash());
		fichier.setNom(updateFichierDTO.getNom());
		fichier.setVersion(updateFichierDTO.getVersion());
		fichier.setHistoriques(historiques);
		if (fichier.getOwner() != null) {
			long idUtilisateur = Config.getIdFromUri(updateFichierDTO.getOwner());
			Owner owner = ownerRepository.getOne(idUtilisateur);
			fichier.setOwner(owner);
		}

		return fichierConverter.convertToDto(fichierRepository.saveAndFlush(fichier));
	}

	/**
	 * Méthode getHistoriques : retourne les historiques concernant un fichier
	 * 
	 * @param id
	 * @return ResponseEntity<Historique>
	 */
	@GetMapping(path = "/{id}/historiques", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HistoriqueDTO> getHistoriques(@PathVariable long id) {
		log.info("GET Fichiers - Historiques - ");
		GenericEntityNotFoundException.checkById(id, fichierRepository, Fichier.class);
		List<HistoriqueDTO> historiques = new ArrayList<>();
		fichierRepository.getOne(id).getHistoriques()
				.forEach(historique -> historiques.add(historiqueConverter.convertToDto(historique)));
		return historiques;
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer un fichier
	 * 
	 * @param long id - ID du fichier à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Fichier - ");
		try {
			fichierRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, Fichier.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer ce fichier", e);
		}

	}

}
