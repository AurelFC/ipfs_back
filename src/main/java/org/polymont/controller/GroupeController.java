package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.polymont.converter.FichierConverter;
import org.polymont.converter.GroupeConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.GroupeDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Groupe;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.FichierRepository;
import org.polymont.repositories.GroupeRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/groupes")
@CrossOrigin(origins = "*")
@Log4j2
public class GroupeController {

	@Autowired
	GroupeRepository groupeRepository;

	@Autowired
	GroupeConverter groupeConverter;

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	FichierRepository fichierRepository;

	@Autowired
	FichierConverter fichierConverter;

	public GroupeController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GroupeDTO> getAll() {
		log.info("GET GroupesAll - ");
		List<GroupeDTO> groupesDtos = new ArrayList<>();
		groupeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
				.forEach(groupe -> groupesDtos.add(groupeConverter.convertToDto(groupe)));
		return groupesDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public GroupeDTO getById(@PathVariable long id) {
		log.info("GET GroupesById - ");
		GenericEntityNotFoundException.checkById(id, groupeRepository, Groupe.class);
		return groupeConverter.convertToDto(groupeRepository.getOne(id));
	}

	/**
	 * Méthode getUtilisateurs : retourne les utilisateurs concernant un groupe
	 * 
	 * @param id
	 * @return ResponseEntity<Utilisateur>
	 */
	@GetMapping(path = "/{id}/utilisateurs", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UtilisateurDTO> getUtilisateurs(@PathVariable long id) {
		log.info("GET Groupes - Utilisateurs - ");
		GenericEntityNotFoundException.checkById(id, groupeRepository, Groupe.class);
		List<UtilisateurDTO> utilisateurs = new ArrayList<>();
		groupeRepository.getOne(id).getUtilisateurs()
				.forEach(utilisateur -> utilisateurs.add(utilisateurConverter.convertToDto(utilisateur)));
		return utilisateurs;
	}

	/**
	 * Méthode getHistoriques : retourne les fichiers concernant un groupe
	 * 
	 * @param id
	 */
	@GetMapping(path = "/{id}/fichiers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FichierDTO> getFichiers(@PathVariable long id) {
		log.info("GET Groupes - Fichiers - ");
		GenericEntityNotFoundException.checkById(id, groupeRepository, Groupe.class);
		List<FichierDTO> fichiers = new ArrayList<>();
		if (groupeRepository.existsById(id)) {
			fichierRepository.getByOwnerId(id).forEach(fichier -> fichiers.add(fichierConverter.convertToDto(fichier)));
		} else {
			throw new EntityNotFoundException("Le groupe " + id + " n'existe pas.");
		}
		return fichiers;
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter un groupe
	 * 
	 * @param GroupeDTO newGroupe
	 * @return ResponseEntity<Groupe>
	 */
	@PostMapping
	public GroupeDTO add(@RequestBody GroupeDTO newGroupe) {
		log.info("POST Groupe - ");
		// Enregistrement du groupe dans la base
		Groupe groupeEntity = groupeConverter.convertToEntity(newGroupe);
		try {
			groupeRepository.saveAndFlush(groupeEntity);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return groupeConverter.convertToDto(groupeEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'un groupe
	 * 
	 * @param long id - ID du groupe à mettre à jour
	 * @return ResponseEntity<Groupe>
	 */
	@PutMapping(path = "/{id}")
	public GroupeDTO update(@RequestBody GroupeDTO updateGroupeDTO, @PathVariable long id) {
		log.info("PUT Groupe - " + updateGroupeDTO);

		Groupe groupe = groupeRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Groupe.class));
		// Mise à jour de l'groupe
		groupe.setLibelle(updateGroupeDTO.getLibelle());
		return groupeConverter.convertToDto(groupeRepository.saveAndFlush(groupe));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer un groupe
	 * 
	 * @param long id - ID du groupe à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Groupe - ");
		try {
			groupeRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, Groupe.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer ce groupe", e);
		}
	}

	/**
	 * Méthode PUT permettant de retirer un utilisateur du groupe
	 * 
	 * @param id  (correspond à l'id du groupe)
	 * @param id2 (correspond à l'id de l'utilisateur)
	 * @return
	 */
	@PutMapping(path = "/{id}/utilisateurs/{id2}")
	public GroupeDTO deleteUtilisateur(@PathVariable long id, @PathVariable long id2) {
		log.info("DELETE Utilisateur du groupe - ");
		Groupe groupe = groupeRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Groupe.class));
		groupe.deleteUtilisateurs(utilisateurRepository.getOne(id2));
		return groupeConverter.convertToDto(groupeRepository.saveAndFlush(groupe));
	}

}
