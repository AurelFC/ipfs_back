package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.polymont.converter.TypeNotificationConverter;
import org.polymont.dtos.TypeNotificationDTO;
import org.polymont.entities.TypeNotification;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.TypeNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/typeNotifications")
@CrossOrigin(origins = "*")
@Log4j2
public class TypeNotificationController {

	@Autowired
	TypeNotificationRepository typeNotificationRepository;

	@Autowired
	TypeNotificationConverter typeNotificationConverter;

	public TypeNotificationController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TypeNotificationDTO> getAll() {
		log.info("GET NotificationsAll - ");
		List<TypeNotificationDTO> typeNotificationsDtos = new ArrayList<>();
		typeNotificationRepository.findAll().forEach(typeNotification -> typeNotificationsDtos
				.add(typeNotificationConverter.convertToDto(typeNotification)));
		return typeNotificationsDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public TypeNotificationDTO getById(@PathVariable long id) {
		log.info("GET TypeNotificationsById - ");
		GenericEntityNotFoundException.checkById(id, typeNotificationRepository, TypeNotification.class);
		return typeNotificationConverter.convertToDto(typeNotificationRepository.getOne(id));
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter une typenotification
	 * 
	 * @param TypeNotificationDTO newTypeNotification
	 * @return ResponseEntity<TypeNotification>
	 */
	@PostMapping
	public TypeNotificationDTO add(@RequestBody TypeNotificationDTO typeNotificationDto) {
		log.info("POST TypeNotification - ");
		TypeNotification typeNotificationEntity = typeNotificationConverter.convertToEntity(typeNotificationDto);
		try {
			typeNotificationRepository.saveAndFlush(typeNotificationEntity);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return typeNotificationConverter.convertToDto(typeNotificationEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'une typenotification
	 * 
	 * @param long id - ID du typenotification à mettre à jour
	 * @return ResponseEntity<TypeNotification>
	 */
	@PutMapping(path = "/{id}")
	public TypeNotificationDTO update(@RequestBody TypeNotificationDTO typeNotificationDto, @PathVariable long id) {
		log.info("PUT TypeNotification - ");

		TypeNotification privilege = typeNotificationRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, TypeNotification.class));
		privilege.setLibelle(typeNotificationDto.getLibelle());
		return typeNotificationConverter.convertToDto(typeNotificationRepository.saveAndFlush(privilege));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer une typenotification
	 * 
	 * @param long id - ID du typenotification à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE TypeNotification - ");
		try {
			typeNotificationRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, TypeNotification.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer ce typenotification", e);
		}
	}

}
