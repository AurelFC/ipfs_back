package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.polymont.configuration.Config;
import org.polymont.converter.NotificationConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.NotificationDTO;
import org.polymont.entities.Notification;
import org.polymont.entities.Utilisateur;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.NotificationRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/notifications")
@CrossOrigin(origins = "*")
@Log4j2
public class NotificationController {

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	NotificationConverter notificationConverter;

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	public NotificationController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<NotificationDTO> getAll() {
		log.info("GET NotificationsAll - ");
		List<NotificationDTO> notificationsDtos = new ArrayList<>();
		notificationRepository.findAll()
				.forEach(notification -> notificationsDtos.add(notificationConverter.convertToDto(notification)));
		return notificationsDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public NotificationDTO getById(@PathVariable long id) {
		log.info("GET NotificationssById - ");
		GenericEntityNotFoundException.checkById(id, notificationRepository, Notification.class);
		return notificationConverter.convertToDto(notificationRepository.getOne(id));
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter une notification
	 * 
	 * @param NotificationDTO newNotification
	 * @return ResponseEntity<Notification>
	 */
	@PostMapping
	public NotificationDTO add(@RequestBody NotificationDTO notificationDto) {
		log.info("POST Notification - ");
		Notification notificationEntity = notificationConverter.convertToEntity(notificationDto);
		try {
			notificationRepository.saveAndFlush(notificationEntity);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return notificationConverter.convertToDto(notificationEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'une notification
	 * 
	 * @param long id - ID de la notification à mettre à jour
	 * @return ResponseEntity<Notification>
	 */
	@PutMapping(path = "/{id}")
	public NotificationDTO update(@RequestBody NotificationDTO updateNotificationDTO, @PathVariable long id) {
		log.info("PUT Notification - ");
		Notification notification = notificationRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Notification.class));

		// Mise à jour de la notification
		notification.setContenu(updateNotificationDTO.getContenu());
		notification.setDateNotif(updateNotificationDTO.getDateNotif());
		notification.setLue(updateNotificationDTO.isLue());
		if (notification.getUtilisateur() != null) {
			long idUtilisateur = Config.getIdFromUri(updateNotificationDTO.getUtilisateur());
			Utilisateur utilisateur = utilisateurRepository.getOne(idUtilisateur);
			log.info("utilisateur : " + utilisateur);
			notification.setUtilisateur(utilisateur);
		}

		return notificationConverter.convertToDto(notificationRepository.saveAndFlush(notification));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer une notification
	 * 
	 * @param long id - ID de la notification à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Notification - ");
		try {
			notificationRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, Notification.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer cette notification", e);
		}
	}

}
