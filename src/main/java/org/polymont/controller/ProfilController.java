package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.polymont.converter.PrivilegeConverter;
import org.polymont.converter.ProfilConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.dtos.ProfilDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Privilege;
import org.polymont.entities.Profil;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.PrivilegeRepository;
import org.polymont.repositories.ProfilRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/profils")
@CrossOrigin(origins = "*")
@Log4j2
public class ProfilController {

	@Autowired
	ProfilRepository profilRepository;

	@Autowired
	ProfilConverter profilConverter;

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	PrivilegeRepository privilegeRepository;

	@Autowired
	PrivilegeConverter privilegeConverter;

	public ProfilController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProfilDTO> getAll() {
		log.info("GET ProfilsAll - ");
		List<ProfilDTO> profilsDtos = new ArrayList<>();
		profilRepository.findAll().forEach(profil -> profilsDtos.add(profilConverter.convertToDto(profil)));
		return profilsDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfilDTO getById(@PathVariable long id) {
		log.info("GET ProfilsById - ");
		GenericEntityNotFoundException.checkById(id, profilRepository, Profil.class);
		return profilConverter.convertToDto(profilRepository.getOne(id));
	}

	/**
	 * Méthode getUtilisateurs : retourne les utilisateurs concernant un profil
	 * 
	 * @param id
	 * @return ResponseEntity<UtilisateurDTO>
	 */
	@GetMapping(path = "/{id}/utilisateurs", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UtilisateurDTO> getUtilisateurs(@PathVariable long id) {
		log.info("GET Utilisateurs - ");
		GenericEntityNotFoundException.checkById(id, profilRepository, Profil.class);
		List<UtilisateurDTO> utilisateurs = new ArrayList<>();
		profilRepository.getOne(id).getUtilisateurs()
				.forEach(utilisateur -> utilisateurs.add(utilisateurConverter.convertToDto(utilisateur)));
		return utilisateurs;
	}

	/**
	 * Méthode getPrivileges : retourne les privileges concernant un profil
	 * 
	 * @param id
	 * @return ResponseEntity<PrivilegeDTO>
	 */
	@GetMapping(path = "/{id}/privileges", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PrivilegeDTO> getPrivileges(@PathVariable long id) {
		log.info("GET Privileges - ");
		GenericEntityNotFoundException.checkById(id, profilRepository, Profil.class);
		List<PrivilegeDTO> privileges = new ArrayList<>();
		profilRepository.getOne(id).getPrivileges()
				.forEach(privilege -> privileges.add(privilegeConverter.convertToDto(privilege)));
		return privileges;
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter un profil
	 * 
	 * @param ProfilDTO profilDto
	 * @return ResponseEntity<ProfilDTO>
	 */
	@PostMapping
	public ProfilDTO add(@RequestBody ProfilDTO profilDto) {
		log.info("POST Profil - ");
		Profil profilEntity = profilConverter.convertToEntity(profilDto);
		try {
			profilRepository.save(profilEntity);
		} catch (ConstraintViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Certaines informations obligatoires sont manquantes", e);
		}
		return profilConverter.convertToDto(profilEntity);
	}

	/**
	 * Méthode POST pour ajouter un privilege à un profil
	 * 
	 * @param id
	 * @param newPrivilegeDTO
	 * @return
	 */

	@PostMapping(path = "/{id}/privileges", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfilDTO addPrivileges(@PathVariable long id, @RequestBody PrivilegeDTO newPrivilegeDTO) {
		log.info("POST Profil - Privileges ajout - ");
		Profil profil = profilRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Profil.class));
		Privilege priv = privilegeConverter.convertToEntity(newPrivilegeDTO);
		if (priv.getId() == 0) {
			privilegeRepository.saveAndFlush(priv);
		}
		profil.addPrivileges(priv);
		profil = profilRepository.saveAndFlush(profil);
		List<PrivilegeDTO> privileges = new ArrayList<>();
		profil.getPrivileges().forEach(privilege -> privileges.add(privilegeConverter.convertToDto(privilege)));
		return profilConverter.convertToDto(profil);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'un profil
	 * 
	 * @param long id - ID du profil à mettre à jour
	 * @return ResponseEntity<Profil>
	 */
	@PutMapping(path = "/{id}")
	public ProfilDTO update(@RequestBody ProfilDTO updateProfilDTO, @PathVariable long id) {
		log.info("PUT Profil - ");
		Profil profil = profilRepository.findById(id)
				.orElseThrow(() -> new GenericEntityNotFoundException(id, Profil.class));
		List<PrivilegeDTO> privileges = new ArrayList<>();
		privileges.addAll(getPrivileges(id));
		profil.setLibelle(updateProfilDTO.getLibelle());

		return profilConverter.convertToDto(profilRepository.saveAndFlush(profil));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer un profil
	 * 
	 * @param long id - ID du profil à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Profil - ");
		try {
		profilRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new GenericEntityNotFoundException(id, Profil.class);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Impossible de supprimer ce profil", e);
		}
	}

}
