package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class UtilisateurDTO extends OwnerDTO{

	private long id;
	private String nom;
	private String prenom;
	private String emailPro;
	private String telPro;
	private String profil;
	private String fichiers;
	private String agences;
	private String groupes;

}