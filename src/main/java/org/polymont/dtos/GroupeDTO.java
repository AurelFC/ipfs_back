package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GroupeDTO extends OwnerDTO{

	private long id;
	private String libelle;
	private String utilisateurs;
	private String fichiers;

}
