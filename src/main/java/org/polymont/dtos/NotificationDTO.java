package org.polymont.dtos;

import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationDTO {

	private Long id;
	private String contenu;
	private Calendar dateNotif;
	private boolean lue;
	private String utilisateur;
	private String typeNotification;

}
