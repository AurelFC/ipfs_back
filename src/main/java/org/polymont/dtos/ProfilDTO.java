package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfilDTO {

	private long id;
	private String libelle;
	private boolean actif;
	private String utilisateurs;
	private String privileges;

}
