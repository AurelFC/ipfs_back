package org.polymont.dtos;

import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriqueDTO {

	private Long id;
	private Calendar dateModif;
	private String libelle;
	private String fichier;

}
