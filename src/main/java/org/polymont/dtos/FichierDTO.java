package org.polymont.dtos;

import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FichierDTO {

	private long id;
	private String nom;
	private String hash;
	private String version;
	private Calendar dateAjout;
	private String extension;
	private String owner;
	private String historique;

}