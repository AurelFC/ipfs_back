package org.polymont.exceptions;

import javax.persistence.PersistenceException;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class GenericEntityNotFoundException extends PersistenceException {

	private static final long serialVersionUID = -1535873541745629528L;

	public GenericEntityNotFoundException(long id, Class<?> cl) {
		super("L'entité " + cl.getSimpleName() + " n°" + id + " n'existe pas.");

	}

	public GenericEntityNotFoundException(String email, Class<?> cl) {
		super("L'entité " + cl.getSimpleName() + " ayant l'email " + email + " n'existe pas.");
	}
	
	public GenericEntityNotFoundException(String message) {
		super(message);
	}

	public static void checkById(long id, JpaRepository<?, Long> repo, Class<?> c) {
		if (!repo.existsById(id)) {
			throw new GenericEntityNotFoundException(id, c);
		}
	}

}