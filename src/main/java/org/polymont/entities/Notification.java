package org.polymont.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notification implements Serializable {

	private static final long serialVersionUID = -746599162104724455L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	private String contenu;

	@NotNull
	private Calendar dateNotif;

	@NotNull
	private boolean lue;

	@ManyToOne
	private TypeNotification typeNotification;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Utilisateur utilisateur;

	/**
	 * @param contenu
	 * @param dateNotif
	 * @param lue
	 * @param typeNotification
	 * @param utilisateur
	 */
	public Notification(@NotBlank String contenu, @NotNull Calendar dateNotif, @NotNull boolean lue,
			TypeNotification typeNotification, Utilisateur utilisateur) {
		super();
		this.contenu = contenu;
		this.dateNotif = dateNotif;
		this.lue = lue;
		this.typeNotification = typeNotification;
		this.utilisateur = utilisateur;
	}

	public String toString() {
		return Config.URI_NOTIFICATIONS + this.id;
	}
}
