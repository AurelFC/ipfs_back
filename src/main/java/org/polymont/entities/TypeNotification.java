package org.polymont.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeNotification implements Serializable {

	private static final long serialVersionUID = 8402306370164967914L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	private String libelle;

	/**
	 * @param libelle
	 */
	public TypeNotification(@NotBlank String libelle) {
		super();
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return Config.URI_TYPENOTIFICATION + this.id;
	}

}
