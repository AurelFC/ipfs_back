package org.polymont.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("G")
public class Groupe extends Owner implements Serializable {

	private static final long serialVersionUID = -5097351887744179835L;

	@NotBlank
	@Column(unique = true)
	private String libelle;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH })
	private Set<Utilisateur> utilisateurs;

	public void addUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs == null) {
			this.utilisateurs = new HashSet<>();
		}

		this.utilisateurs.add(utilisateur);
	}

	public void deleteUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs != null) {
			this.utilisateurs.remove(utilisateur);
		}
	}

	@Override
	public String toString() {
		return Config.URI_GROUPES + this.getId();
	}

}
