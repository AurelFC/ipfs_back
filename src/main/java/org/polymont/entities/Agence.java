package org.polymont.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("A")
public class Agence extends Owner implements Serializable {

	private static final long serialVersionUID = -806818872358804282L;

	@NotBlank
	@Column(unique = true)
	private String libelle;
	
	@NotNull
	private boolean actif;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH })
	private Set<Utilisateur> utilisateurs;

	public void addUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs == null) {
			this.utilisateurs = new HashSet<>();
		}

		this.utilisateurs.add(utilisateur);
	}

	public void deleteUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs != null) {
			this.utilisateurs.remove(utilisateur);
		}
	}

	@Override
	public String toString() {
		return Config.URI_AGENCES + this.getId();
	}

}
