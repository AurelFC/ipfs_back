package org.polymont.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="owner_type")
public abstract class Owner implements Serializable {

	private static final long serialVersionUID = -4466228389600084109L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private boolean actif;

	public Owner() {
		super();
	}

	public Owner(long id, boolean actif) {
		super();
		this.id = id;
		this.actif = actif;
	}

}
