package org.polymont.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Privilege implements Serializable {

	private static final long serialVersionUID = 4564972585748144588L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	@Column(unique = true)
	private String libelle;

	/**
	 * @param libelle
	 * @param profils
	 */
	public Privilege(@NotBlank String libelle) {
		super();
		this.libelle = libelle;
	}

	public String toString() {
		return Config.URI_PRIVILEGES + this.id;
	}
}
