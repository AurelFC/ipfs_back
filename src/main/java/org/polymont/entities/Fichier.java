package org.polymont.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.polymont.configuration.Config;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Fichier implements Serializable {

	private static final long serialVersionUID = 228831875691089728L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	private String nom;

	@NotBlank
	@Column(unique = true)
	private String hash;

	@NotBlank
	private String version;

	@DateTimeFormat
	private Calendar dateAjout;

	@NotBlank
	private String extension;

	@ManyToOne
	@NotNull
	private Owner owner;

	@OneToMany(mappedBy = "fichier", cascade = CascadeType.REMOVE)
	private List<Historique> historiques;

	/**
	 * @param nom
	 * @param hash
	 * @param version
	 * @param dateAjout
	 * @param extension
	 * @param utilisateur
	 * @param historiques
	 */
	public Fichier(@NotBlank String nom, @NotBlank String hash, @NotBlank String version, Calendar dateAjout,
			@NotBlank String extension, Owner owner, List<Historique> historiques) {
		super();
		this.nom = nom;
		this.hash = hash;
		this.version = version;
		this.dateAjout = dateAjout;
		this.extension = extension;
		this.owner = owner;

		if (historiques != null) {
			this.historiques = historiques;
		} else {
			this.historiques = new ArrayList<>();
		}
	}

	@Override
	public String toString() {
		return Config.URI_FICHIERS + this.id;
	}

	public void addHistorique(Historique histo) {
		if (this.historiques == null) {
			this.historiques = new ArrayList<>();
		}

		this.historiques.add(histo);
	}
}
