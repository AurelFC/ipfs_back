package org.polymont.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("U")
public class Utilisateur extends Owner implements Serializable {

	private static final long serialVersionUID = 1291913906682762873L;

	@NotBlank
	private String nom;

	@NotBlank
	private String prenom;

	@Email
	@NotBlank
	@Column(unique = true)
	private String emailPro;

	private String telPro;

	@NotNull
	private boolean actif;

	@ManyToOne
	private Profil profil;

	@ManyToMany(mappedBy = "utilisateurs", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH })
	private List<Groupe> groupes;

	@ManyToMany(mappedBy = "utilisateurs", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH })
	private List<Agence> agences;

	public void addGroupes(Groupe groupe) {
		if (this.groupes == null) {
			this.groupes = new ArrayList<>();
		}

		this.groupes.add(groupe);
	}

	public void addAgences(Agence agence) {
		if (this.agences == null) {
			this.agences = new ArrayList<>();
		}

		this.agences.add(agence);
	}

	@Override
	public String toString() {
		return Config.URI_UTILISATEURS + this.getId();
	}

}
