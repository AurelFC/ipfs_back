package org.polymont.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Profil implements Serializable {

	private static final long serialVersionUID = -1491932107959856393L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	@Column(unique = true)
	private String libelle;

	@NotNull
	private boolean actif;

	@OneToMany(mappedBy = "profil")
	private List<Utilisateur> utilisateurs;

	@ManyToMany
	private List<Privilege> privileges;

	public void addUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs == null) {
			this.utilisateurs = new ArrayList<>();
		}

		this.utilisateurs.add(utilisateur);
	}

	public void addPrivileges(Privilege privilege) {
		if (this.privileges == null) {
			this.privileges = new ArrayList<>();
		}

		this.privileges.add(privilege);
	}

	public String toString() {
		return Config.URI_PROFILS + this.id;
	}

}
