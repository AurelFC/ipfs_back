package org.polymont.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Historique implements Serializable {

	private static final long serialVersionUID = -1593683117096260510L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	private Calendar dateModif;

	@NotBlank
	private String libelle;

	@ManyToOne
	Fichier fichier;

	/**
	 * @param dateModif
	 * @param libelle
	 * @param fichier
	 */
	public Historique(@NotNull Calendar dateModif, @NotBlank String libelle, Fichier fichier) {
		super();
		this.dateModif = dateModif;
		this.libelle = libelle;
		this.fichier = fichier;
	}
	
	/**
	 * @param dateModif
	 * @param libelle
	 */
	public Historique(@NotNull Calendar dateModif, @NotBlank String libelle) {
		super();
		this.dateModif = dateModif;
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return Config.URI_HISTORIQUES + this.id;
	}

}
