package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.dtos.TypeNotificationDTO;
import org.polymont.entities.TypeNotification;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class TypeNotificationConverter implements Converter<TypeNotification, TypeNotificationDTO> {

	@Autowired
	private ModelMapper mapper;

	public TypeNotificationConverter() {
		super();
	}

	@Override
	public TypeNotificationDTO convertToDto(TypeNotification typeNotification) {
		log.info("Convert to dto TypeNotification");
		return this.mapper.map(typeNotification, TypeNotificationDTO.class);
	}

	@Override
	public TypeNotification convertToEntity(TypeNotificationDTO typeNotificationDto) {
		log.info("Convert to dto Entity TypeNotification");
		return this.mapper.map(typeNotificationDto, TypeNotification.class);
	}

}
