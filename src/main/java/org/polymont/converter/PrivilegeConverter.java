package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.entities.Privilege;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class PrivilegeConverter implements Converter<Privilege, PrivilegeDTO> {

	@Autowired
	private ModelMapper mapper;

	public PrivilegeConverter() {
		super();
	}

	@Override
	public PrivilegeDTO convertToDto(Privilege privilege) {
		log.info("Convert to dto Privilege");
		return this.mapper.map(privilege, PrivilegeDTO.class);
	}

	@Override
	public Privilege convertToEntity(PrivilegeDTO privilegeDto) {
		log.info("Convert to entity Privilege");
		return this.mapper.map(privilegeDto, Privilege.class);
	}

}
