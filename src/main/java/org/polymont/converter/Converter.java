package org.polymont.converter;

public interface Converter<T, E> {
	public E convertToDto(T t);
	public T convertToEntity(E e);
}
