package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.dtos.ProfilDTO;
import org.polymont.entities.Profil;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ProfilConverter implements Converter<Profil, ProfilDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private UtilisateurRepository utilisateurRepository;


	public ProfilConverter() {
		super();
	}

	@Override
	public ProfilDTO convertToDto(Profil profil) {
		log.info("Convert to dto Profil");
		ProfilDTO profilDto = this.mapper.map(profil, ProfilDTO.class);
		profilDto.setUtilisateurs("/profils/" + profil.getId() + "/utilisateurs");
		profilDto.setPrivileges("/profils/" + profil.getId() + "/privileges");
		return profilDto;
	}

	@Override
	public Profil convertToEntity(ProfilDTO profilDto) {
		log.info("Convert to entity Profil" + profilDto.getPrivileges());
		
		Profil profil = this.mapper.map(profilDto, Profil.class);
		profil.setUtilisateurs(utilisateurRepository.getByProfilId(profilDto.getId()));
		return profil;
	}

}
