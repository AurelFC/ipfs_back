package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.configuration.Config;
import org.polymont.dtos.AgenceDTO;
import org.polymont.entities.Agence;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class AgenceConverter implements Converter<Agence, AgenceDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	public AgenceConverter() {
		super();
	}

	@Override
	public AgenceDTO convertToDto(Agence agence) {
		log.info("Convert to dto Agence");
		AgenceDTO agenceDto = this.mapper.map(agence, AgenceDTO.class);
		agenceDto.setUtilisateurs(agence.toString() + Config.URI_UTILISATEURS);
		agenceDto.setFichiers(agence.toString() + Config.URI_FICHIERS);
		return agenceDto;
	}

	@Override
	public Agence convertToEntity(AgenceDTO agenceDto) {
		log.info("Convert to entity Agence");
		Agence agence = this.mapper.map(agenceDto, Agence.class);

		if (agenceDto.getUtilisateurs() != null) {
			agence.setUtilisateurs(utilisateurRepository.getByAgencesId(agenceDto.getId()));
		}

		return agence;
	}

}
