package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.configuration.Config;
import org.polymont.dtos.NotificationDTO;
import org.polymont.entities.Notification;
import org.polymont.repositories.TypeNotificationRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class NotificationConverter implements Converter<Notification, NotificationDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	private TypeNotificationRepository typeNotificationRepository;


	public NotificationConverter() {
		super();
	}

	@Override
	public NotificationDTO convertToDto(Notification notification) {
		log.info("Convert to dto Notification");
		return this.mapper.map(notification, NotificationDTO.class);
	}

	@Override
	public Notification convertToEntity(NotificationDTO notificationDto) {
		log.info("Convert to entity Notification");
		Notification notification = this.mapper.map(notificationDto, Notification.class);
		
		if (notificationDto.getUtilisateur() != null) {
			notification.setUtilisateur(utilisateurRepository.getOne(Config.getIdFromUri(notificationDto.getUtilisateur())));
		}
		
		if (notificationDto.getTypeNotification() != null) {
			notification.setTypeNotification(
					typeNotificationRepository.getOne(Config.getIdFromUri(notificationDto.getTypeNotification())));
		}

	
		return notification;
	}

}
