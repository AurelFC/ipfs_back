package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.configuration.Config;
import org.polymont.dtos.FichierDTO;
import org.polymont.entities.Fichier;
import org.polymont.repositories.FichierRepository;
import org.polymont.repositories.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class FichierConverter implements Converter<Fichier, FichierDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private OwnerRepository ownerRepository;

	@Autowired
	private FichierRepository fichierRepository;

	public FichierConverter() {
		super();
	}

	@Override
	public FichierDTO convertToDto(Fichier fichier) {
		log.info("Convert to dto Fichier");
		FichierDTO fichierDto = this.mapper.map(fichier, FichierDTO.class);
		fichierDto.setHistorique(fichier.toString() + Config.URI_HISTORIQUES);
		return fichierDto;
	}

	@Override
	public Fichier convertToEntity(FichierDTO fichierDto) {
		log.info("Convert to entity Fichier");
		Fichier fichier = this.mapper.map(fichierDto, Fichier.class);
		if (fichierDto.getOwner() != null) {
			fichier.setOwner(ownerRepository.getOne(Config.getIdFromUri(fichierDto.getOwner())));
		}
		if (fichierDto.getId() != 0L) {
			fichier.setHistoriques(fichierRepository.getOne(fichierDto.getId()).getHistoriques());
		}
		return fichier;
	}

}
