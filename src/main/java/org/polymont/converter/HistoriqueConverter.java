package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.configuration.Config;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.entities.Historique;
import org.polymont.repositories.FichierRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class HistoriqueConverter implements Converter<Historique, HistoriqueDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private FichierRepository fichierRepository;

	public HistoriqueConverter() {
		super();
	}

	@Override
	public HistoriqueDTO convertToDto(Historique historique) {
		log.info("Convert to dto Historique");
		return this.mapper.map(historique, HistoriqueDTO.class);
	}

	@Override
	public Historique convertToEntity(HistoriqueDTO historiqueDto) {
		log.info("Convert to entity Historique");

		Historique historique = this.mapper.map(historiqueDto, Historique.class);

		if (historiqueDto.getFichier() != null) {
			historique.setFichier(fichierRepository.getOne(Config.getIdFromUri(historiqueDto.getFichier())));
		}

		return historique;
	}

}
