INSERT INTO groupe
	(libelle)
VALUES
	("MAPA"),
	("MACIF"),
	("ARKEA"),
	("COMMERCIAUX");
	
INSERT INTO agence
	(libelle)
VALUES
	("NIORT"),
	("BREST"),
	("NANTES");
	
INSERT INTO profil
	(libelle)
VALUES
	("ADMIN"),
	("USER"),
	("GUEST");
	
	INSERT INTO privilege
	(libelle)
VALUES
	("VISUALISER"),
	("TELECHARGER"),
	("UPLOADER"),
	("SUPPRIMER"),
	("PARTAGER");

INSERT INTO profil_privileges
	(privileges_id, profil_id)
VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1);
	
INSERT INTO utilisateur
	(email_pro, nom, prenom, tel_pro,historique_id,profil_id)
VALUES
    ('axel.dromer@polymont.fr', 'Dromer', 'Axel', '0505050505', 2),
    ('elodie.guiard@polymont.fr', 'Guiard', 'Elodie', '0505050505', 2),
    ('mathis.perron@polymont.fr', 'Perron', 'Mathis', '0505050505', 2 ),
    ('adrien.bremaud@polymont.fr', 'Bremaud', 'Adrien', '0505050505', 2),
    ('guillaume.delahaye@polymont.fr', 'Delahaye', 'Guillaume', '0505050505', 2),
    ('nicolas.rousseau@polymont.fr', 'Rousseau', 'Nicolas', '0505050505', 2),
    ('christian.rodriguez@polymont.fr', 'Rodriguez', 'Christian', '0505050505', 2),
    ('y.bendi-ouis@polymont.fr', 'Bendi-Ouis', 'Yannis', '0505050505', 2),
    ('wael.bouharb@polymont.fr', 'Bouharb', 'Wael', '0505050505', 2),
    ('thibaud.moulet@polymont.fr', 'Beerman', 'Joker', '0505050505', 2),
    ('a.fuerte-canivet@polymont.fr', 'Fuerte-Canivet', 'Aurelien', '0505050505', 1),
    ('jeremy.gelineau@polymont.fr', 'Gelineau', 'Jeremy', '0505050505', 2),
    ('romuald.andre@polymont.fr', 'Andre', 'Romuald', '0505050505', 2),
    ('bobby@polymont.fr', 'Bobby', 'Michel', '0505050505', 3);
    
INSERT INTO fichier
	(date_ajout, extension, hash, nom, version, utilisateur_id)
VALUES
	(NULL, NULL, NULL, NULL, NULL, NULL);


INSERT INTO agence_utilisateurs
	(agences_id, utilisateurs_id)
VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(1, 6),
	(1, 7),
	(1, 8),
	(1, 9),
	(1, 10),
	(1, 11),
	(1, 12),
	(1, 13),
	(2, 1),
	(2, 14);



INSERT INTO groupe_utilisateurs
	(groupes_id, utilisateurs_id)
VALUES
	(1, 1),
	(1, 2),
	(2, 3),
	(3, 4),
	(2, 5),
	(1, 6),
	(2, 7),
	(3, 8),
	(1, 9),
	(2, 10),
	(3, 11),
	(1, 12),
	(2, 13),
	(2, 1),
	(4, 14);
	
INSERT INTO type_notification
	(libelle)
VALUES
	("Partage"),
	("Systeme"),
	("Personnelle");


INSERT INTO notification
	(contenu, date,lue, utilisateur)
VALUES
	("Vous avez partagé le fichier X", '2020-01-21T12:00:01', 1),
	("Changement de mot de passe effectué", '2020-02-21T12:00:01', 2),
	("Michel a partagé le fichier X", '2020-03-21T12:00:01', 1),
	("Vous avez rejoint le groupe RH", '2020-01-21T12:00:01', 3),
	("Vous avez partagé le fichier Y", '2020-02-21T12:00:01', 1),
	("Jean-Pierre a mis à jour le fichier Y", '2020-01-21T12:00:01', 1);
	

	










