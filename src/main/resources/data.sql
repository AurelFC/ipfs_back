INSERT INTO owner
	(owner_type, actif)
VALUES
	('G', true),
	('G', true),
	('G', true),
	('G', true),
	('A', true),
	('A', true),
	('A', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true),
	('U', true);

INSERT INTO groupe 
	(libelle,id)
VALUES
	('MAPA',1),
	('MACIF',2),
	('ARKEA',3),
	('COMMERCIAUX',4);
	
INSERT INTO agence
	(libelle,id)
VALUES
	('NIORT',5),
	('BREST',6),
	('NANTES',7);

INSERT INTO profil
	(libelle, actif)
VALUES
	('ADMIN', true),
	('USER', true),
	('GUEST', true);
	
INSERT INTO privilege
	(libelle)
VALUES
	('VISUALISER'),
	('TELECHARGER'),
	('UPLOADER'),
	('SUPPRIMER'),
	('PARTAGER');

INSERT INTO profil_privileges
	(privileges_id, profil_id)
VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1);
	
INSERT INTO utilisateur
	(email_pro, nom, prenom, tel_pro,profil_id,id)
VALUES
    ('axel.dromer@polymont.fr', 'Dromer', 'Axel', '0505050505', 2,  8),
    ('elodie.guiard@polymont.fr', 'Guiard', 'Elodie', '0505050505', 2,  9),
    ('mathis.perron@polymont.fr', 'Perron', 'Mathis', '0505050505', 2,  10),
    ('adrien.bremaud@polymont.fr', 'Bremaud', 'Adrien', '0505050505', 2, 11),
    ('guillaume.delahaye@polymont.fr', 'Delahaye', 'Guillaume', '0505050505', 2,  12),
    ('nicolas.rousseau@polymont.fr', 'Rousseau', 'Nicolas', '0505050505', 2,  13),
    ('christian.rodriguez@polymont.fr', 'Rodriguez', 'Christian', '0505050505', 2, 14),
    ('y.bendi-ouis@polymont.fr', 'Bendi-Ouis', 'Yannis', '0505050505', 2, 15),
    ('wael.bouharb@polymont.fr', 'Bouharb', 'Wael', '0505050505', 2, 16),
    ('thibaud.moulet@polymont.fr', 'Beerman', 'Joker', '0505050505', 2, 17),
    ('a.fuerte-canivet@polymont.fr', 'Fuerte-Canivet', 'Aurelien', '0505050505', 1, 18),
    ('jeremy.gelineau@polymont.fr', 'Gelineau', 'Jeremy', '0505050505', 2, 19),
    ('romuald.andre@polymont.fr', 'Andre', 'Romuald', '0505050505', 2,  20),
    ('bobby@polymont.fr', 'Bobby', 'Michel', '0505050505', 3,  21);
	
INSERT INTO agence_utilisateurs
	(agences_id, utilisateurs_id)
VALUES
	(5, 8),
	(5, 9),
	(5, 10),
	(5, 11),
	(5, 12),
	(5, 13),
	(5, 14),
	(5, 15),
	(5, 16),
	(5, 17),
	(5, 18),
	(5, 19),
	(5, 20),
	(7, 8),
	(7, 9),
	(7, 10),
	(6, 8),
	(6, 9);



INSERT INTO groupe_utilisateurs
	(groupes_id, utilisateurs_id)
VALUES
	(1, 8),
	(1, 9),
	(2, 10),
	(3, 11),
	(2, 12),
	(1, 13),
	(2, 14),
	(3, 15),
	(1, 16),
	(2, 17),
	(3, 18),
	(1, 19),
	(2, 20),
	(2, 21),
	(3, 12),
	(4, 8),
	(4, 20),
	(4, 21);
	
INSERT INTO type_notification
	(libelle)
VALUES
	('Partage'),
	('Systeme'),
	('Personnelle');
		
INSERT INTO fichier
	(date_ajout, extension, hash, nom, version, owner_id)
VALUES
	('2020-01-21T12:00:01', 'pdf', 'adadfaf15', 'test', '1', 1),
	('2020-01-21T12:00:01', 'pdf', 'adadfaf16', 'dfv', '1', 2),
	('2020-01-21T12:00:01', 'pdf', 'adadfaf17', 'tesvfvt', '1', 3),
	('2020-01-21T12:00:01', 'csv', 'adadfaf18', 'tagada0', '1', 5),
	('2020-01-21T12:00:01', 'csv', 'adadfaf19', 'tagada', '1', 18);
	
INSERT INTO historique
	(date_modif, libelle, fichier_id)
VALUES
	('2018-01-21T12:00:01', 'ceci est un historique', 1),
	('2020-02-21T12:00:01', 'ceci est un historique', 1),
	('2020-03-21T12:00:01', 'ceci est un historique', 2),
	('2020-01-21T12:00:01', 'ceci est un historique', 2),
	('2020-02-21T12:00:01', 'ceci est un historique', 3),
	('2020-01-21T12:00:01', 'ceci est un historique', 3);
	

	
INSERT INTO notification
	(contenu, date_notif, lue, utilisateur_id, type_notification_id)
VALUES
	('Vous avez partagé le fichier X', '2019-02-21T12:00:01', true, 8, 1),
	('Changement de mot de passe effectué', '2019-02-21T12:00:01', true, 9, 2),
	('Michel a partagé le fichier X', '2019-03-21T12:00:01', false, 10, 1),
	('Vous avez rejoint le groupe RH', '2019-02-21T12:00:01', false, 11, 3),
	('Vous avez partagé le fichier Y', '2019-02-21T12:00:01', true, 12, 1),
	('Jean-Pierre a mis à jour le fichier Y', '2019-03-21T12:00:01', true, 13, 1);
	


	
	
	