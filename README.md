# *Back du projet IPFS.*

## 1. Specs techniques : 
 * Java version 12
 * BDD : MariaDB
 * Maven
 * Spring Boot 2.2.2
 * lombok (ajoute des annotations permettant d'alleger le code)
 * JUnit + H2 database pour les tests 

## 2. Conception :

### 2.1 Pattern

On utilise le pattern DTO (Data Transfer Object), qui permet de simplifier le transfert de données entre les différents microservices. Le DTO n'a pas de comportement, 
sauf pour le stockage et la récupération de ses propres données (accesseurs et mutateurs). Il est intéressant lorsqu'il est couplé avec une architecture REST car dans 
le cas de laisons entre entités (ex : un profil pour un utilisateur), le DTO va renvoyer l'URI de la ressource (ici le profil).

Ci- dessous l'affichage (JSON) de la requête à l'url:  http:// *** /utilisateurs/1

 {
 
    * "id": 1,
    * "nom": "Nom",
    * "prenom": "Prenom",
    * "emailPro": "prenon.nom@polymont.fr",
    * "telPro": "0505050505",
    * "profil": "/profils/2",
    * "fichiers": "/utilisateurs/1/fichiers/",
    * "agences": "/utilisateurs/1/agences/",
    * "groupes": "/utilisateurs/1/groupes/"
    
 } 

Afin de convertir les entités en DTO, on utilise un ModelMapper pour lier les attributs de l'entité aux types primitifs du DTO. 

### 2.2 Architecture

Etant un projet web, l'architecture choisie est REST. 

