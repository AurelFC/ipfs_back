FROM maven:3.6-jdk-13

# création du dossier cible
RUN mkdir /usr/src/project

WORKDIR /usr/src/project

COPY . .

# création des sources compilées dans ./target
RUN mvn package

WORKDIR /usr/src/

RUN mv ./project/target/ipfs_back.jar .

# Alléger l'image
RUN rm -rf project/

EXPOSE 8080
CMD ["java", "-jar", "ipfs_back.jar"]